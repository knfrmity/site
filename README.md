# knfrmity site

website for essays, reading lists, and found content

https://site.knfrmity.space

all from an ml perspective

built with hugo and papermod theme

# developing and building

Clone the repo, taking care to clone submodules as well. The theme is one submodule, the static site is another (the `pages` branch of this repo, under `./public`).

```shell
git clone --recurse-submodules https://codeberg.org/knfrmity/site.git
# or
git clone https://codeberg.org/knfrmity/site.git
cd site
git submodule update --init --recursive
```

Modify config (`hugo.yml`), theme (see https://github.com/adityatelange/hugo-PaperMod for documentation), and site content (under `./content`) as necessary and build the site.

I prefer to build the site using a Docker container running Hugo, for a persistent host install of Hugo see the installation instructions here: https://gohugo.io/installation/.

Docker site build:

```shell
knfrmity@pc:~/.../site$ docker run --rm -v $PWD:/src klakegg/hugo:ext
```

Then commit and push the `./public` submodule folder to update the static website .

To run a local copy of the site on the built-in Hugo web server for testing or local development at http://localhost:1313:

```shell
knfrmity@pc:~/.../site$ docker run --rm -v $PWD:/src -p 1313:1313 klakegg/hugo:ext serve
```
