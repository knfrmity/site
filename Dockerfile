FROM klakegg/hugo:ext as Builder
WORKDIR /src
COPY . .
RUN hugo build

FROM lipanski/docker-static-website:latest as Server
#   or if arm64 is the target system change for this line
#FROM knfrmity/static-website-arm64:latest as Server
COPY --from=Builder /src/public .
COPY httpd.conf .
