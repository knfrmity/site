---
weight: 1
title: "Karl Marx in Our Time"
tags: ["found content", "aes"]
---

At a flea market in Berlin I found a volume of speeches from the International Theoretical Conference of the Central Committee of the Socialist Unity Party of Germany, which took place in Berlin on the 11 to 16 April 1983. The conference was themed and titled "Karl Marx and Our Time: The Struggle for Peace and Social Progress".

The conference was themed around the contributions of Marx to the international working class struggle as a commemoration of the centenary of his death.

After looking for the book online, and only finding vague references to it, I thought I would take the time to transcribe it, speech for speech, for the benefit of all interested and as a window into the not too distant past of the class struggle.


The volume was published by Zeit in Bild Verlag Dresden, 1983

From the introduction of the publication:

> An International Theoretical Conference organized by the Central Committee of the Socialist Unity Party of Germany took place in Berlin, capital of the GDR, from 11 to 16 April 1983. The theme of the conference was Karl Marx and Our Time: The Struggle for Peace and Social Progress. The participants were 145 communist and worker's parties, revolutionary vanguard parties, national revolutionary and liberation movements, socialist and social democratic parties from 111 countries.

> Representatives of parties working underground also participated. In the course of the conference, 140 delegates took the floor or submitted written contributions. This collection contains both the manuscripts released by the participants for publication and which in most cases are more comprehensive than the speech delivered at the conference, and the contributions submitted in writing.

> The English translation by Intertext, Foreign Language Service of the GDR, was based on the German translation wherever no English text was available.

In transcribing I have done my best to remain true not only to source content but source formatting. Misspellings and grammatical errors from the source material have been left as they were.

Footnotes are both from the source material and at times my own thoughts while transcribing the pages.

An index of the speeches contained in the volume is forthcoming. Specific requests can be addressed via the site repo or on Lemmygrad.
