---
weight: 4
title: "Giacondo Dias - Brazilian Communist Party"
url: marx-in-our-time/dias
description: "An overview of the theory and practise of the Brazilian Communist Party and its position on the international workers movement and struggle for peace."
tags: ["brazil", "peace", "national liberation", "marx in our time", "found content"]
date: 2023-12-18
---

Giocondo Dias


General Secretary, Central Committee

Brazilian Communist Party


Comrades and Friends,

We regard this conference as a gathering of historic importance representing an outstanding contribution to the struggle for social progress and peace. We therefore wish to extend our greetings to you and, above all, to thank the Socialist Unity Party of Germany for providing the opportunity of our coming together. This initiative launched by the comrades of the SED clearly illustrates their consistence in applying the Theses proposed for the Karl Marx Year, because this conference is dedicated to peace. “Peace, after all, is the decisive condition for the continued existence of humanity and the prime factor in solving all society’s other problems.”[^1]

It is also in line with their understanding that “over recent years, the movement composed of the most wide-ranging peace forces has broken all previous social, political and ideological barriers.”[^2]

As everywhere in the capitalist world, the well-known centres of anti-communist ideological struggle in Brazil, too, have used the occasion of the centenary of Marx’s death to unleash a frenzied campaign against Marxism-Leninism. This campaign is given wide room in the mass media, in scientific publications and in the various political spheres. On both an international and national scale, the centres are under the pressure of the severe crisis of capitalism and the all-enveloping force emanating from the rebellion of the exploited and oppressed, from democratic and Progressive aspirations which are in stark conflict with the society of the “free world”, its way of life and its ideological a and cultural products.

Whatever the way chosen, it always boils down to one intention: to split the working masses and the forces of democracy and progress from Marxism and the real core that gives it its unique revolutionary character and world-historic significance and represents the mainstay of its overall conception&mdash;*the inseparably practice-related theory ff the revolutionary transformation of the world by the working class* as a *categoric expression* of the dialectical understanding of the essence and transformation of social relations in capitalism.

The hotchpotch rhetoric uttered by the Marx critics is aimed at doing away with or erasing the essence of Marxism, the *centrepiece* of its theory and practice, its permanent "threat” and impulse. They believe that they can thus break its backbone and deny the importance of Marx's life and work in general, which doubtlessly unceasingly and unreservedly, and *in defiance of all distortions* were dedicated to one course&mdash;*the course of the revolution.*

In other words, they hope to destroy the foundation on which Marx’s critique of the character and boundaries of the philosophy, political economy historiography and politics of his day is based, and seek at the same time to alienate the category which Marx used as his starting point to answer the problems facing social science then and now and which established Marxism as a new and revolutionary, openly *partisan* world outlook that has brought about changes in thinking, class struggle and the world itself.

This wish is not exactly modest: elimination of the basis and foundation of the Marxist prediction of the world-historic mission of the proletariat, its understanding of history as a process in which one socio-economic formation is superseded (through class struggle) by another, its consistent efforts to merge the workers’ movement with the socialist movement and to forge an organizational unity between the working class, the revolutionary party and revolutionary theory.

The aim is to refute the one factor that has made Marxism a “guideline for action”, has turned it into a theory that has awakened the militancy and passion of the working class and of millions upon millions of men and women, and has made Marxism into the most powerful intellectual force of our time ; the factor that illustrates Marxism’s universal importance, most clearly shows that Leninism is its consistent continuation and qualitative enrichment, and proves that Marxism-Leninism is the Marxism of our epoch ; the one element revealing that the struggle for peace, democracy and social progress and its undisputable achievements are the truly contemporary inheritance of Marx. It is, however, clear that the vanguard forces represented by the socialist countries, especially the Soviet Union, and the international revolutionary workers' movement with the communist parties as their core are its incontestable creations and heirs.

*Observing the anniversary of Marx's death in a revolutionary way*

For us, the observance of the centenary of Marx's death, which virtually coincides with the 61st anniversary of the founding of the Brazilian Communist Party, is not merely an academic or rhetorical matter, but it is determined by the revolutionary obligation. It allows a broad view of Marxism, its theoretical mainstays and methods in many spheres, and the discussion of many new issues. It means their application to the problems relating to the theory and tactics if the Brazilian revolution, including the issues arising from leading, in the first place, the struggle for peace, the struggle for democracy and social progress in our country and world-wide. One must not forget, however, to include the tasks of overcoming the military regime, the unity, organization and mobilization of the working class, its system of alliances and the achievement of democracy, the shaping and legalisation of the Brazilian Communist Party, and the consolidation and further development of Marxist-Leninist theory in our country.

From the dialectical perspective of Brazilian reality, and with the aim of changing it in the direction of freedom, the topicality of Marx and Marxism-Leninism, their whole power and open spirit of renewal are invincible, and for this reason they stir the souls of the revolutionaries and arouse the hopes of those who have come aware of the injustice of capitalism.

Indeed, none of the problems our people is faced with can be comprehended, nor can their essential connections be studied as a subject of thinking, in order to grasp their development and formulate predictions without the theory, categories and methods of Marx and Lenin. Moreover, all this cannot be understood without the application and further elaboration of the theory by the communist parties and their theoreticians, especially when it comes to those aspects related to the Marxist theory of accumulation, class struggle, state and revolution, Lenin’s theory of imperialism, the theory of state monopolist capitalism, and of the general crisis of capitalism and the revolution in the epoch of imperialism, all of which form the basis for a scientific approach to the questions of peace and war, the scientific and technological revolution, the internationalization of capital, the new balance of forces in the world (due to the consolidation and advance of the world socialist system, the forces of peace, democracy and national liberation), the new conditions under which struggle is taking place, the theory of the united front, the new relationship between capitalism, democracy, revolution and socialism, the ideological struggle and the new problems arising for philosophy, political economy, history, etc.

The vitality if Marxism-Leninism in Brazil, which has again put the communist question on the agenda with great energy, results from nothing but the fact that the Brazilian communists, in drafting the theory and tactics of the Brazilian revolution, are concentrating their efforts increasingly on the persistent struggle against both "left"-wing and right-wing dogmatism and revisionism.

In view of the methodological foundations of Marxism-Leninism, its regarding the origin and development of capitalism, its social order, ideology and alienating character, regarding social classes and the state, imperialism and its general crisis, and the revolution and its dynamism, our chief goal is to find the way theoretically and practically that will enable "the proletariat to first of all acquire political supremacy..."[^3] and to constitute "...the leading class of the nation..."[^4] so that in Brazil, too, "... private property drives itself...towards its own dissolution..."[^5] by producing the proletariat.

However, we do not stress the proletariat merely for the sake of principle or moral reasons, or in order to cite the classics, but in the first place because this is in accordance with Marx's teachings and the historical experience concerning the course of the contradiction between the social character of production and the private capitalist form of appropriation prevailing in our country as well. Facts are making it increasingly obvious that in Brazil “... (the) productive powers already acquired and the existing social relations should no longer be capable of existing side by side.”[^6]

This is the conflict that fully confirms the roles of the bourgeoisie and the proletariat as the two fundamental classes in Brazilian society and that underlies their whole current movement. In other words: *in our country the revolutionary epoch of transition from capitalism to socialism has been initiated.*

*The democratic and the national path of the Brazilian revolution*

In contrast to the ideas of the “ultra-left” in Brazil and all those who give a vulgar interpretation of Marx, the revolution does not stem *directly and immediately* from the dialectics of the productive forces and the social relations of production but from a far more complex process involving the entirety of basis and superstructure, the economy, ideology and politics as well as a number of external factors, i.e. the process of class struggle.

Although the contradiction between the social character of production and the private capitalist form of appropriation is openly and irrevocably forcing the working class (and the bourgeoisie) to follow their goals and take historical action&mdash;regardless of how these should be understood at a given moment&mdash;it cannot be looked upon in its motion, i.e. in a *linear* way even if it can be found only at the basis.

On the one hand, capitalism&mdash;being a mode of production&mdash;unites the working class, the bourgeoisie and the big landowners and places them in opposition to each other, while on the other, it leads to gradual differences and contradictions within the propertied classes due to the process of centralisation of capital and land. The law of the accumulation of capital formulated by Marx is classical in its understanding of the dialectics of this motion within the basis of bourgeois society itself. By considering only the abstract motion of the basis, Marx was able to recognize the process of monopolization of capital (the process which Lenin used as a starting point in formulating his theory of imperialism) and pointed out that the “capital tycoons" within the bourgeoisie represent simultaneously the major obstacle to the development of the capitalist mode of production itself and a factor of its negation. Moreover, capitalism, if only the basis is considered, exists in a concrete form only as a socio-economic formation, which puts not only the proletariat, the bourgeoisie and the big landowners but also other classes and interests in opposition to each other,

Proceeding from the *Communist Manifesto*, Marx and Engels stated that from a political point of view the ruling classes are not always diametrically opposed to the proletariat.

Applying this method, which was brilliantly applied by Lenin and has been developed by the world communist movement mainly in connection With the struggle against the major enemy and the policy of the united front, we find not only that capitalist development in Brazil is producing “... the material conditions of a new society...”,[^7] but also that its structure reveals the fundamental elements making up its gross contradiction to democracy and to social and national interests (elements which, once overcome, guarantee the “future of the movement”): imperialism, spearheaded by North America, and the domestic monopolies and landed property, whose mechanisms are organically merging with those of the state. On the other hand, the development of the class struggle in the country has brought forth two groups of forces whose opposition towards each other determines the essence of the political process and its course: the reactionary group which seeks to sell out the country and is spearheaded by the alliance between imperialism, especially in North America, and internal reaction, and the democratic and national group embracing the proletariat, the farmers, the petty bourgeois and the new middle strata, which from a historical point of view, banks on certain sections of the bourgeoisie.

This struggle determines the entirety of strategic tasks to be fulfilled by the proletariat in order to complete the transition to socialism and to conduct the struggle on whose outcome the success of that transition depends.

Consequently, the topicality of socialism in Brazil under the given conditions, which are yet another confirmation of the topicality of Marx and Marxism-Leninism, and the democratic and national road of transition to socialism that the Brazilian revolution has to take are the result not only of objective factors, of the severity of the basic contradiction characterizing the capitalist and monopolist socio-economic formation which is dependent upon imperialism and integrates landed property, and of the processes shaking the capitalist world but also of the development of subjective elements marked by the struggle for democracy, for national interests, social progress and peace. Through this battle, the forces of the world of labour and culture, the new middle strata and other social sections are intervening increasingly in national political life, but their views are becoming detached from the pureley political aspects and are turning specifically against the brutal and estranged social order of bourgeois society.

A growing number of revolutionary elements capable of representing are and applying progressive social ideas and ensuring the transition to socialism are organising themselves in this manner. The working class in particular is not only expanding its independent organization but is also giving increasing priority in its society-organizing activities to its own goal and spirit. Indeed, large sections of the proletariat are "... already conscious of (their) historic task..." and are "... constantly working to develop that consciousness into complete clarity.”[^8] They overcame the condition under which the workers’ "... political understanding concealed from them the roots of social distress... falsified their insight into their real aim...*deceived* their *social instinct*.[^9]

They have become "combatants for socialism”, and this applies not only to the workers.

Hence, the linchpin of the theory and tactics of the Brazilian communists at present lies in endeavours to lead this process to its climax. The organization of the bloc of democratic and national forces under the leadership of the working class and its ability to hold its own in Brazilian life are the *indispensable* prerequisites for the transition from capitalism to socialism through anti-imperialist, antimonopolist, anti-oligarchic transformations, the conquest of political power, the establishment of a democracy of the masses, a democratic, independent and progressive development of the national economy, the cultural revolution and a consistent foreign policy for peace, anti-imperialism and non-alignment.

The achievement of these significant objectives will, of course, be impossible without a powerful communist mass party, without consolidating, deepening and disseminating the Marxist-Leninist theory throughout the country by mastering the theoretical foundations and methods and by improvements in their creative application.

From this point of view, we confirm the central position occupied in the Brazilian revolution by the question of democracy and, consequently, the central role of the working class resulting exactly from the magnitude of the tasks to be tackled, the ever more evident contradiction between capitalism and democracy, and the necessity of educating the proletariat and the working masses by all-embracing thorough, wise and continued actions in all parts of the political arena. Imperialism and internal reaction are trying to maintain their economic power and social oppression by undermining and liquidating democracy, as has been adequately proved since 1964 by the state and regime that installed themselves in the country. The point, however, is not only this but a logic and principled position, to quote Lenin who followed Marx on this, as on all other issues. Lenin teaches that "... it is precisely complete liberty that the proletariat, as the foremost champion of democracy, is striving to attain" because both "... the immediate interests of the proletariat and...the 'final aims of socialism'..." need "...the fullest possible measure of political liberty...".[^10]

Lenin always despised those who underrated the political freedoms: "Whoever wants to reach socialism by any other path than that of political democracy, will inevitably arrive at conclusions that are absurd and reactionary both in the economic and political sense."[^11]

He recognized that the organization of millions of proletarians, socialist education and the emergence of a collective spirit among them through a revolutionary programme depend on "the fullest possible achievement of democratic transformations."[^12] At the same time he stressed the necessity of the vanguard of the proletariat being more strongly represented in the struggle for democracy even if the reaction and liberals are waving the banner of democratism”, exactly as they are doing in Brazil right now.

In agreement with the best traditions of Marx, Engels and Lenin, we reject the formally restricted struggle for democracy conducted in isolation from the struggle for socio-economic changes which alone can ensure and develop democracy. We go beyond the confined political frame of this struggle and lay emphasis on its social character. In other words, it has to be taken as far as possible, up to socialism. We resist the attitude of those who swear that socialism can be established by the evolution&mdash;and not revolution&mdash;of bourgeois democracy, those who defend a kind of "democratism which keeps within the limits of what is permitted by the police and not permitted by logic."[^13] “But *socialism* cannot be realised without *revolution*”[^14]

It is because we understand that “the antagonism between the proletariat and the bourgeoisie is a struggle of class against class, a struggle which carried to its highest expression is a total revolution,”[^15] and that, “only in an order of things in which there are no more classes and class antagonisms that *social evolutions* will cease to be *political revolutions*.”[^16], that we do not ally with "vulgar democracy, which sees the millenium in the democratic republic and has no suspicion that it is precisely in this last form of state bourgeois society that the class struggle has to be fought out to a conclusion."[^17]

That is why we value the great and universal historical importance of bourgeois democracy, which the “ultra-left” cannot understand. It is *unique in its character*, in contrast to what the reformists think it to be. The Brazilian proletariat is in the can of the struggle for bourgeois democracy which it needs even more than the bourgeoisie and which it is striving for in order to wage successfully its final struggle against capital.

We believe that in drafting the entirety if these tasks our party and our people are making an ever more important contribution to the safeguarding of world peace, which, to cite Erich Honecker at the opening of this conference, is the fundamental problem of our time. However, and this we are well aware of, we are not doing this without purposeful involvement in the struggle for peace as the chief task which, over and above this, offers an unprecedented scope for the revolutionary activities of the Brazilian proletariat. The defence of peace forges more than any other issue the unity of interest between the various democratic and national forces of our country and with similarly minded people the world over shows them vividly their common enemy, the necessity of uniting without delay, the necessity of joint action to prevent the holocaust imperialism wishes to inflict on mankind.

The forces of war under the insane direction of the Reagan administration have altered the international framework of détente by their bellicose policy of provocation and aggression. In Latin America they seek to militarize relations between the Latin American states by fuelling chauvinism. This has already led to several conflicts in the region. Furthermore, they are increasingly militarizing their relations to the Latin American peoples. Terrible cases in point are the armed aggression in Argentina, on the Falkland islands and the North American actions in Central America and the Caribbean, which are manifesting themselves in the interference in the internal affairs of El Salvador, in the provocations launched against Cuba and the war that has already been unleashed against Nicaragua. Thwarting this policy is an indispensable prerequisite for the preservation of peace in the world, not only in America. We are not saying this for the sake of exaggeration, but to alarm this conference and the peace-loving forces world-wide. Let us recall that it was the so-called missile crisis in Latin America that confronted mankind in 1962 with the direct threat of a nuclear conflagration due to an imperialist policy designed to reverse revolutionary progress on our continent.

We fully agree with the comrades of the Socialist Unity Party of Germany: "Currently, the struggle for peace is spearheaded by the fight for agreements on halting armament, arms limitation and disarmament on the basis of equality and equal security, and above all by actions to prevent the deployment of US medium-range missiles in Western Europe.”[^8]

There is no doubt that now more than ever the fate of mankind hinges on the solution of this very question.

We Brazilian communists are convinced that we&mdash;the world-wide movement of the various peace-loving forces&mdash;will be able to give a positive reply to the challenge of those who are driving mankind towards war if we curb the most aggressive imperialist forces, mobilize our peoples, and unite, all of us, in the peace movement. This movement has proved over and over again that it is able to prevent war, and it will be able to achieve even more if joint actions are carried out on the broadest possible front and with firm determination. It obvious that none of this can be achieved if elbowroom is given to the forces if war and reaction, if no struggle is waged against those who are truing to divide the peace movement and bring some of its sections into opposition to the socialist countries by desseminating the lie of a “Soviet military threat".

So multifarious as the peace movement has become, so much as it has gained both in originality and breadth, it still rests mainly on the active and consistent peace-making power of socialism, especially in the Soviet Union. This is quite naturally so, because the world socialist system is a creation of Marx's theory, of the proletariat inspired by Marxism-Leninism.

Thank you for your attention.


[^1]: Theses proposed by the Central Committee of the SED for Karl Marx Year 1983, p. 23

[^2]: Ibid,. p. 24


[^3]: Karl Marx/Friedrich Engels. Manifesto of the Communist Party. In: Karl Marx/Friedrich Engels, Selected Works, Vol. 1, p. 124

[^4]: Ibid.

[^5]: Karl Marx/Friedrich Engels. The Holy Family. In: Karl Marx/Friedrich Engels, Collected Works, Vol. 4, p. 36

[^6]: Karl Marx. The Poverty of Philosophy. In: Karl Marx/Friedrich Engels, Collected Works Vol. 6, p. 211

[^7]: Karl Marx. Moralising Criticism and Critical Morality. In: Karl Marx/Friedrich Engels, Collected Works, Vol. 6, p. 320

[^8]: Karl Marx/Friedrich Engels. The Holy Family. In: Karl Marx/Friedrich Engels, Collected Works, Vol. 4, p. 37

[^9]: Conf. Karl Marx. Critical Marginal Notes on the Article “The King of Prussia and Social Reform. By a Prussian”. In: Karl Marx/Friedrich Engels, Collected Works, Vol. 3, p. 204

[^10]: V.I. Lenin. Two Tactics of Social-Democracy in the Democratic Revolution. In: V. I. Lenin, Collected Works, Vol. 9, p. 25

[^11]: Ibid., p. 29

[^12]: Ibid., p. 29

[^13]: Critique of the Gotha Programme. In: Karl Max/Friedrich Engels, Selected Works, Vol. 3, p. 27

[^14]: Karl Marx. Critical Marginal Notes on the Article “The King of Prussia and Social Reform. By a Prussian”. In: Karl Marx/Friedrich Engels, Collected Works, Vol. 3, p. 206

[^15]: Karl Marx. The Poverty of Philosophy. In: Karl Marx/Friedrich Engels, Collected Works, Vol. 6, p. 212

[^16]: Ibid. p. 212

[^17]: Karl Marx. Critique of the Gotha Programme. In: Karl Marx/Friedrich Engels, Selected Works, Vol. 3, p. 27

[^18]: Theses proposed by the Central Committee of the SED for Karl Marx Year 1983, p. 23
