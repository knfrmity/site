---
title: "Kim Chung Rn, Korean Worker's Party"
url: marx-in-our-time/kim
description: "A speech relating the importance of Marx's work and an overview of the contemporary global struggle against imperialism."
tags: ["korea", "aes", "marx in our time", "found content"]
date: 2023-12-10
---

Kim Chung Rin


Member, Politbureau,

Secretary, Central Committee,

Korean Workers’ Party


Distinguished Comrades,

At first, I'd like to thank you, distinguished Comrade Erich Honecker, for the invitation to our delegation to the International Theoretical Conference on the occasion of the 165th birth anniversary and the death centenary of Karl Marx.

The delegation of the Workers’ Party of Korea is pleased to participate in this conference and pays the highest tribute to the great leader and teacher of the international working class, Karl Marx, for his immortal service to the cause of the liberation of mankind.

Karl Marx is a great thinker and theoretician who founded for the first time the doctrine on scientific communism.

Having engaged in his activities during the days of rapid development of industrial capitalism and of preparation for revolution by the working class, Marx made a deep analysis to the requirements of the times and the then social relations on this basis propounded Marxism, thus gave scientific answers to socio-historical problems that had been placed on the order of monopoly capitalist era.

The greatest exploits performed by Marx for the human history on ideology is the elucidation of dialectic materialism whereby he proved the inevitability of ruin of capitalism and of victory of socialism and communism and as result, socialism was converted from imaginary conception to a scientific theory.

Marx put an end to the viewpoint of idealism related to correlation between material and consciousness, existence and thought on the basis of summing up all the previous philosophical thought and socio-historical outlook ranging from ancient philosophy of Democritus an Epicurus to recent philosophy of Hegel and Feuerbach and established dialectic materialistic outlook and socio-historical outlook that material is primary to all and that universal matters change and evolute according to the law of material motion.

Marx understood the process of social development as a natural process of material motion and accordingly expounded the law governing evolution of capitalist economy and on this basis gave comprehensive answers to the law on birth, development and collapse of the capitalist production mode.

In his works the *Capital* compiled after dedication of a great part of his life he made a profound analysis to the production relations of capitalist society in a clear logical terms and discovered that surplus value production is the absolute law in the capitalistic way of production by which he thoroughly unmasked the exploitation nature of capitalist system, the source of poverty of the working class and contradictions existing in capitalist society.

With propounding the surplus value theory by Marx, the revolutionary truth was discovered that capitalism is, in the long run, ruined by the struggle of the working class because the capitalist production relations pose themselves as fetters of progress for production and society at a certain stage of its development,

Having realized the great might of the oppressed and humiliated poor masses of proletariat which was unnoticed by anybody, Marx defined the working class as the most advanced and revolutionary class capable for burying bourgeois society and building socialism and communism and made clear its historical mission for the first time.

The declaration of Marx that “In the communist revolution proletarians have nothing to lose but chains while gaining the whole world”[^1] instilled to the working class and popular masses firm confidence in victory and far-reaching hopes and ambition for the future and thereby arousing them to rise up as one in the sacred struggle to destroy the bulwark of capital and build a new society.

What holds important place in Marx’s doctrine is the theory on class struggle and proletariat dictatorship.

Marx closely studied the struggle of the working class for nearly half a century ranging from Chartist movement up to the revolt of the Silesian textile workers, 1848-1850 revolution and heroic struggle of Paris working class and at last drew the conclusion that the history of mankind is a history of class struggle and the class struggle constitutes motive force of class society and manifested that the struggle of the working class would inevitably develop into a social revolution to overthrow the capitalist system and finally bring about the proletariat dictatorship.

Marx also advanced the theory on the party if the working class and its tactics that the working class and the popular masses should have a party, its revolutionary vanguard and be led under its leadership of they want to win victory in a life and death struggle against capitalist class armed with to the teeth.

The theory of Marx on the proletariat dictatorship and party's tactics of the working class became a most powerful weapon serving as an outstanding theory which provided an basic guarantee for transition from capitalism to socialism and communism and enabling the working class to implement its historical mission.

Thanks to the creation of scientific communist doctrine by Marx the "eternity" of capitalism was broken into smithereens and consequently a broad avenue has come to open for the international working class and the popular masses to cast off the chains of capital and achieve freedom and liberation.

Indeed, the creation of Marxism was a knell heralding the downfall of the capitalist society and a world-historic event which ushered in the the ae international communist movement.

Marx was an outstanding thinker and theoretician as well as practician possessed of strong will, unfatique energy and extraordinary organizational ability.

He devoted all his energy to enlighten the working class and rouse them to the struggle visiting one factory after the other in different parts of Germany, Paris and London even under the pursuing persecution by the enemy and poverty.

Marx together with Engels put up such militant slogan as “Workers of the whole world, unite!” in the *Communist Manifesto*, the first programme of the international working class and strove to tally the working class of all countries under its banner actively inspiring and encouraging them in various forms of revolutionary struggle. And he finally founded the First International, the first working-class party, thus developing their struggle against capital onto a new phase.

The foundation of the First International and the struggle for its expansion and development are the proud review in his all political activities and a significant exploit conducive to the development of the international communist movement.

Marxism and its cause have been defended and emerged victorious amidst a fierce struggle against all hues of reactionary bourgeois theories and various opportunism.

Bourgeoisie and their servants were engaged in all sorts of despicable moves to emasculate and strangulate Marxism and its cause in their cradle bearing a implacable hatred for them.

However, each time the loser was not Marxism but they themselves.

Thanks to the uncompromising struggle on the part of Marx, Engels, Lenin and other successors loyal to his cause the purity of Marxism has been defended and Marxism enriched and developed into Marxism-Leninism, a powerful weapon of the struggle against capitalism for the cause of socialism and communism.

Really the life of Marx was a fruitful one of an outstanding thinker and theoretician and a great revolutionary who had initiated the revolutionary idea of the working class and paved for the first time the revolutionary cause of proletariats and his name will go down for good in the history of international communist and working-class movements.

Comrades,

Over one century has elapsed since Karl Marx conducted activities, and the revolutionary cause of the working class has made a great headway.

Our transition from capitalism to socialism started with the victory of Great October Revolution in Russia under the leadership of V. I. Lenin, has been rapidly pushed ahead after the 2nd World War and is now progressed extensively and profoundly on a world-wide scale.

Socialism has been expanded into world-wide scale beyond the limit of one country and the looks of the world have been changed radically.

Socialism also emerged victorious on the land of China whose population is one billion, as is blossoming in Cuba of Western Hemisphere under the very nose of the US imperialists and demonstrating its immortal vitality in many other parts of the world.

Today socialism is striking roots deep on the land of Germany, motherland of Marx, too.

The existence of the developed and peace-loving German Democratic Republic is playing an important role in averting a war and ensuring peace and security in Europe.

We warmly hail the successes registered by the working class and people of the German Democratic Republic in their endeavours for the grandeur and development of the country and betterment of the well-being of the people under the leadership of Comrade Erich Honecker and the Socialist Unity Party of Germany and wish them greater successes in their future struggle for building a developed socialist society.

The whole course of the Korean revolution full of arduous ordeals yet heroic events was the one of the one of the glorious victory traversed under the banner of of ever-victorious Marxism-Leninism and the banner of the immortal Tschutschhe[^2] ideology.

The Korean people under the wise leadership of the great leader Comrade Kim Il Sung have launched a sacred struggle for freedom and independence and carved out a new history of creation and construction, while holding fast to the Chajusong (tr. independence).

Today the Democratic People’s Republic of Korea has been converted into an independent socialist state with modern industry and developed rural economy, brilliant national culture and mighty self-reliant defensive power and is reliably defending the eastern post of socialism.

Under the ever-growing impact of socialism fierce flames of the liberation struggle are sweeping over the vast areas of Asia, Africa and Latin America and under this flames a great number of countries have already achieved national independence and are turning out as one in the struggle for creating a new society and a new life.

Indeed, the old structures of the world based on domination and subjugation are shaking to their very foundation and a new era has arrived when hundreds of millions of popular masses who had been regarded merely as the object of the history for thousands of years have appeared as the masters in the arena of history to shape their own destinies independently and creatively.

However, the revolutionary cause of the working class and the cause of socialism communism prophesied by Marx have not yet been completed and a lot of hardships and ordeals still stand in their way.

Capitalism, already destined to ruin by Marxism, transfigured itself into modern imperialism based on state monopoly capitalism and has long made a last-ditch effort, being on a downfall. But it still remains a dangerous agressive force hindering the independent cause of the world people.

Scared at the increased socialist force and the ever-intensified struggle of the people for indendence against imperialism, the imperialists ate making desperate attempts to maintain and expand the sphere of their supremacy and regain their lost old position by all means.

In particular, the US imperialists, the Chieftain of modern imperialism, are stretching out their talons of aggression to everywhere in the world, resorting to the “policy of strength” in an attempt to find a way out of the serious political and economic crisis in the new war provocations.

Of late, they are intensifying more than ever before criminal aggressive moves in Northeast Asia including the Korean peninsula, the Middle East, Southern Africa and Central America and viciously acting to deploy new medium-range nuclear missiles in Europe and unleash a nuclear war against the socialist countries.

Never before have so extensively as today the imperialists formed allied forces and aggression and war manoeuvres everywhere in the world, making a fuss about “anti-communism”.

The prevailing situation today urgently demands the socialist countries and the communist and workers’ parties wage a more vigorous joint struggle to prevent a global war, safeguard peace and security and achieve the national independence and win a victory in the cause of socialism in firm unity under the banner of Marxism-Leninism and proletarian internationalism and the banner of the anti-imperialist, anti-US struggle.

Unity is the powerful weapon held in the hands of the working class, and the international working class has always emerged victorious by the strength of unity in the struggle against the allied capitalist forces.

The socialist countries and communist and workers’ parties should lay primary emphasis on unity and subordinate everything to it and unite with one another, putting aside differences and finding their denominators.

In order to hold in check and frustrate the imperialists’ policy of aggression and war and safeguard world peace and security of mankind it is imperative to form a broad international anti-imperialist, anti-US common front and take a dynamic anti-imperialist, anti-US concerted action.

If the peoples of the world over bind the US imperialists hand and foot at each position and inflict a collective attack on them, the US imperialists will be impotent and a new phase be opened up in the anti-imperialist, anti-US struggle.

As in the the past so in the future, too, the Workers’ Party of Korea and the Korean people will vigorously fight on for the global independence in unity with the socialist countries and the working class of the world, unity with the non-aligned countries, the world oppressed nations, with the democratic and peace movements and in firm unity with all the progressive peoples of the world.

The revolutionary cause of the working class, the cause of socialism and communism will surely emerge victorious as Karl Marx, the great teacher, wished and the exploits he performed for mankind will be immortal.

Long live the ever-victorious banner of Marxism!



[^1]: Karl Marx/Friedrich Engels: Manifesto of the Communist Party. In: K. Marx/F. Engels, Collected Works, Vol. 6, p. 496

[^2]: Transcription note: Tschutschhe would have been the GDR German transliteration of 주체사상, or Juche as currently romanized by the ROK and as known in most English speaking countries. The likely official DPRK romanization as well as the FRG German transliteration is Chuch’e.
