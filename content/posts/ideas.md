---
title: "post ideas"
draft: true
---


## Topics

Projection
 - colonial / anti-colonial
  - decolonialism portrayed as just as violent as colonialism
  - if the colonized aren't savages, does that mean we're the baddies?
  - https://lemmygrad.ml/comment/2965933
 - propaganda tropes
 - rule of law / human rights / democracy
  - west claims to respect these things, but breaks their own rules constantly
  - claims that while opposed states may have these things on paper, it's meaningless in practice
    - Marx, Critique of Gotha: "One sees that such hollow phrases are the foundations of society, etc. One sees that such hollow phrases can be twisted and turned as desired."

Fascism
 - conflation of nazism with fascism
 - fascism is colonialism brought back home
 - what the nazis did wrong
 - what the EU & US did right
 - neoliberalism is fascist
 - lebensraum = manifest destiny = white man's burden = humanitarian intervention

Speeches from SED conference
