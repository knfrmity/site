---
title: "knfrmity"
---

# Welcome

A collection of my own thoughts, found content, and maybe some cross posts. All written or selected from an ML perspective.

This site is hosted on Codeberg Pages and all of my own content is released here under [CC-BY-NC 4.0](https://www.creativecommons.org/licenses/by-nc/4.0/deed.en).

Comments, corrections, suggestions and the like are welcome via the [site repository](https://codeberg.org/knfrmity/site) (as an issue or PR) or as comments or messages via [Lemmygrad](https://lemmygrad.ml/u/knfrmity).
